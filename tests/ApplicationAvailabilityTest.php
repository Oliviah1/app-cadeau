<?php
/**
 * Created by PhpStorm.
 * User: herizo
 * Date: 11/9/18
 * Time: 2:14 PM
 */

namespace Tests;



use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApplicationAvailabilityTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     * @param $url
     */
    public function testUrlIsSuccessful($url)
    {
                    $client = static::createClient();
        $client->request("GET", $url);

        self::assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function urlProvider()
    {
        yield["/"];
        yield["/hello"];
        yield["/form-test"];
        yield["/add-post"];
        yield["/click-me"];
    }
}